<?php
class Controller_Articles extends Controller{

    public function __construct(){
        parent::__construct();
        $this->model = new Model_Articles();
        $this->view = new View();
    }

    public function action_index(){
        $data = array();
        $data = $this->model->get_data();
        $this->view->generate('articles_view.php', 'template_view.php', $data);
    }

    public function action_show($id){
        $data = $this->model->getShow($id);
        $this->view->generate('showArt_view.php','template_view.php', $data);
    }
}