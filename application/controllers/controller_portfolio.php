<?php
class Controller_Portfolio extends Controller{

    public function __construct(){
        parent::__construct();
        $this->model = new Model_Portfolio();
        $this->view = new View();
    }

    public function action_index(){
        $data = array();
        $data = $this->model->get_data();
        $this->view->generate('portfolio_view.php','template_view.php', $data);
    }

    public function action_show($id){
        $data = $this->model->getShow($id);
        $this->view->generate('showPort_view.php','template_view.php', $data);
    }
}