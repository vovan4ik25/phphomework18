<div>
    <ul class="nav nav-tabs" role="tablist">
        <li><a href="/">Главная</a></li>
        <li><a href="portfolio">Portfolio</a></li>
        <li><a href="articles">Articles</a></li>
    </ul>
</div>
<h1>Portfolio page:</h1>
<h2>My Portfolio:</h2>
<ul>
    <?php foreach($data as $work):?>
            <li>
                <?=$work['url'];?>
                <br>
                <?=$work['year'];?>
                <br>
                <a class="btn btn-primary active" href="/portfolio/show/<?=$work['id']?>" role="button">Просмотр</a>
                <br>
                <br>
            </li>
    <?php endforeach;?>
</ul>