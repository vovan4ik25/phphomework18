<div>
    <ul class="nav nav-tabs" role="tablist">
        <li><a href="/">Главная</a></li>
        <li><a href="portfolio">Portfolio</a></li>
        <li><a href="articles">Articles</a></li>
    </ul>
</div>
<h1>Articles page:</h1>
<h2>My Articles:</h2>
<ul>
<?php foreach($data as $work):?>
    <li>
        <?=$work['title'];?>
        <br>
        <a class="btn btn-primary active" href="/articles/show/<?=$work['id']?>" role="button">Просмотр</a>
        <br>
        <br>
    </li>
<?php endforeach;?>
</ul>