<?php
class Model{

    public $db;
    public $pdo;
    public $table;

    public function get_data(){
        
    }

    public function getPDO(){
        try{
            $pdo = new PDO('mysql:host=localhost;dbname=mvc','vovan4ik','25252104');
            $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $pdo->exec("SET NAMES 'utf8'");
        }catch(PDOException $e){
            echo "Не получилось подключиться к Базе Данных.<br>";
            echo $e->getMessage();
            exit();
        }
        return $pdo;
    }


    public function getTable(){
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        $this->table = $routes[1];
        return $this->table;
    }

    public function getDb(){
        try{
            $sql = 'SELECT * FROM '.$this->getTable();//вызов данных из таблици
            $result = $this->getPDO()->query($sql);
            $this->db = $result->fetchAll();
            //$this->db->query($sql);
        }catch(PDOException $e){
            echo "Ошибка получения данных: ".$e->getMessage();
            exit();
        }
        return $this->db;
    }

    public function getShow($id){

    }

}