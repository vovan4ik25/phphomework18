<?php
class Route{

    static function start(){
        $controller_name = "Main";
        $action_name = 'index';
        $argument_name = NULL;

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])){
            $controller_name = $routes[1];
        }

        if (!empty($routes[2])){
            $action_name = $routes[2];
        }

        if (!empty($routes[3])){
            $argument_name = $routes[3];
        }

        $model_name = 'Model_' . $controller_name;

        $controller_name = 'Controller_' . $controller_name;

        $action_name = 'action_' . $action_name;

        $model_file = strtolower($model_name);

        $model_path = 'application/models/' . $model_file . '.php';

        if (file_exists($model_path)){
            include_once $model_path;
        }

        $controller_file = strtolower($controller_name);
        $controller_path = 'application/controllers/' . $controller_file . '.php';
        if (file_exists($controller_path)) {
            include_once $controller_path;
        }else{
            Route::ErrorPage404();
        }

        $controller = new $controller_name;
        $action = $action_name;
        $argument = $argument_name;

        if ($argument == NULL){
            if (method_exists($controller, $action)){
                $controller->$action();
            }else{
                Route::ErrorPage404();
            }
        }elseif ($argument <> NULL){
            if (method_exists($controller, $action)) {
                $controller->$action($argument);
            }else{
                Route::ErrorPage404();
            }
        }
    }

    static function ErrorPage404(){
        die('404');
        //TODO: Make normal 404 page
    }
}