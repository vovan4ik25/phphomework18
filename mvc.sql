-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 22 2017 г., 22:35
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mvc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`) VALUES
(1, 'Безвизовый режим для Украины: как это действует', 'Безвизовый режим Украины с Евросоюзом вступил в силу 11 июня 2017 года.\r\nЗа первые пять дней его действия (по состоянию на 16 июня), по данным Госпогранслужбы, безвизом успешно воспользовались 8690 граждан Украины.\r\nПри этом 1950 граждан, а это 22%, следовали за границу авиационным транспортом. Остальные пользовались железной дорогой или пересекали границу в автомобильных пунктах пропуска на границе с ЕС, большинство из которых на границе с Польшей.\r\n16 украинцам было отказано во въезде, преимущественно из-за нарушения миграционного законодательства стран ЕС во время предыдущего пребывания.\r\nНВ рассказывает правила въезда в ЕС согласно Шенгенскому кодексу про границы, которые начали действовать.'),
(2, 'Конец АТО? Реакция на резонансное заявление Турчинова о необходимости нового формата защиты страны', '13 июня в интервью агентству Интерфакс-Украина глава Совета национальной безопасности и обороны Александр Турчинов заявил о необходимости перейти к новому формату защиты Украины от гибридной агрессии России.\r\nПо его словам, за три года военные действия на Донбассе переросли как по продолжительности, так и по масштабам формат АТО.\r\nТурчинов также заявил, что \"уточненный законопроект\" О восстановлении государственного суверенитета Украины над временно оккупированными территориями Донецкой и Луганской областей будет представлен президенту.\r\nНВ собрало ряд комментариев о резонансном заявлении главы СНБО.');

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `portfolio`
--

INSERT INTO `portfolio` (`id`, `year`, `url`, `description`) VALUES
(1, 2017, 'https://rche.ru/267_php-kirillica-v-regulyarnyx-vyrazheniyax.html', 'Столкнулся я с проблемой, а именно сайт не в какую не хотел поддерживать русские пароли. При регистрации пользователь в качестве пароля мог использовать только цифры и латинские буквы. Но для рунета также актуальна поддержка паролей с использованием кириллицы.'),
(2, 2015, 'https://www.virtualbox.org/wiki/Downloads', 'Here, you will find links to VirtualBox binaries and its source code.');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
